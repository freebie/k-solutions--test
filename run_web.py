import sys
import os

from werkzeug.middleware.proxy_fix import ProxyFix

from web.service import app

sys.path.append(os.getcwd())


# run web admin panel
def run():
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run(host='0.0.0.0', port=2211, debug=True)


if __name__ == '__main__':
    run()
