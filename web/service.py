import hashlib
import config
import requests
import datetime

from flask import Flask, render_template, request, make_response, redirect
from flask_bootstrap import Bootstrap
from flask_mongoengine import MongoEngine

from math import ceil
from .db_model.order import Order


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = config.PROJECT_NAME

app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False

app.config['MONGODB_SETTINGS'] = {
    'db': config.PROJECT_NAME,
    'alias':  'default'
}

Bootstrap(app=app)
MongoEngine(app=app)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        order = Order()

        sum_value = request.form.get('sum')
        if sum_value:
            sum_value = sum_value.replace(',', '.')
            try:
                sum_value = float(sum_value)
                order.amount = sum_value
            except ValueError:
                return make_response(render_template(template_name_or_list='index.html', error='Incorrect input'))
        else:
            return make_response(render_template(template_name_or_list='index.html', error='No sum input'))

        description = request.form.get('description')
        if description:
            order.description = description
        else:
            return make_response(render_template(template_name_or_list='index.html', error='No description input'))

        currency = request.form.get('currency')
        order.currency = currency
        order.date = datetime.datetime.now() + datetime.timedelta(hours=2)
        order.save()
        if currency == 'EUR':
            return make_response(redirect('/eur_payment_form?order_id={0}'.format(order.id)))

        elif currency == 'USD':
            res = _usd_payment_method(order)
            return make_response(redirect(res))

        elif currency == 'RUB':
            return make_response(redirect('/rub_payment_form?order_id={0}'.format(order.id)))

        return make_response(render_template(template_name_or_list='index.html'))

    else:
        return make_response(render_template(template_name_or_list='index.html'))


@app.route('/orders', methods=['GET'])
def orders_method():
    page = int(request.args.get('p', None)) if request.args.get('p', None) else 1


    end = page * 20
    start = end - 20
    orders_len = Order.objects.count()

    p_all = ceil(orders_len / 20) if ceil(orders_len / 20) != 0 else 1

    if start > orders_len:
        start = orders_len

    if end > orders_len:
        end = orders_len

    orders = Order.objects[start:end].order_by('-date')

    result = render_template(template_name_or_list='orders.html', p=page, rows=orders,
                             p_all=p_all,
                             u_all=orders_len)

    return result


@app.route('/eur_payment_form', methods=['GET', 'POST'])
def pay_form_method():
    order_id = request.args.get('order_id')
    order: Order = Order.objects(id=order_id).first()
    if not order:
        make_response(redirect('/'))

    keys_sorted = ['amount', 'currency', 'shop_id', 'shop_order_id']

    params = {
        'amount': order.amount,
        'currency': config.currencies_values[order.currency],
        'shop_id': config.shop_id,
        'shop_order_id': order.id
    }

    hash_string = str()
    for key in keys_sorted:
        hash_string = hash_string + str(params[key]) + ':' if keys_sorted.index(key) != len(keys_sorted)-1 \
            else hash_string + str(params[key])

    hash_string = hash_string + config.secret_key

    sign = hashlib.sha256(hash_string.encode()).hexdigest()

    return make_response(render_template('eur_form.html', order=order, currencies_values=config.currencies_values,
                                         shop_id=config.shop_id, sign=sign))


@app.route('/rub_payment_form', methods=['GET', 'POST'])
def invoice_form_method():
    order_id = request.args.get('order_id')
    order: Order = Order.objects(id=order_id).first()
    if not order:
        make_response(redirect('/'))

    res = _rub_payment_method(order)

    data = res.get('data').get('data')
    method = res.get('data').get('method')
    action = res.get('data').get('url')

    return make_response(render_template('rub_form.html', data=data, method=method, action=action))


#   todo paymants methodss and makes reditrects
def _rub_payment_method(order):
    url = 'https://core.piastrix.com/invoice/create'

    keys_sorted = ['amount', 'currency', 'payway', 'shop_id', 'shop_order_id']

    params = {'amount': order.amount,
              'currency': config.currencies_values[order.currency],
              'payway': config.payway,
              'shop_id': config.shop_id,
              'shop_order_id': str(order.id),
              }

    hash_string = str()
    for key in keys_sorted:
        hash_string = hash_string + str(params[key]) + ':' if keys_sorted.index(key) != len(keys_sorted) - 1 \
            else hash_string + str(params[key])

    hash_string = hash_string + config.secret_key
    sign = hashlib.sha256(hash_string.encode()).hexdigest()

    params['sign'] = sign
    params['description'] = order.description

    res = requests.post(url=url, json=params)

    return res.json()


def _usd_payment_method(order):
    url = 'https://core.piastrix.com/bill/create'

    keys_sorted = ['payer_currency', 'shop_amount', 'shop_currency', 'shop_id', 'shop_order_id']

    params = {'payer_currency': config.currencies_values[order.currency],
              'shop_amount': order.amount,
              'shop_currency': config.currencies_values[order.currency],
              'shop_id': config.shop_id,
              'shop_order_id': str(order.id),
              }

    hash_string = str()
    for key in keys_sorted:
        hash_string = hash_string + str(params[key]) + ':' if keys_sorted.index(key) != len(keys_sorted)-1 \
            else hash_string + str(params[key])

    hash_string = hash_string + config.secret_key
    sign = hashlib.sha256(hash_string.encode()).hexdigest()

    params['sign'] = sign
    params['description'] = order.description

    res = requests.post(url=url, json=params)

    try:
        res = res.json().get('data').get('url')
    except Exception:
        res = None

    return res

