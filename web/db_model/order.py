from mongoengine import Document, StringField, FloatField, DateTimeField


class Order(Document):
    currency = StringField()
    amount = FloatField()
    date = DateTimeField()
    description = StringField()
